package com.example.a20210818_swathi_jatavattu_nycschools.repository

import androidx.lifecycle.MutableLiveData
import com.example.a20210818_swathi_jatavattu_nycschools.response.SATScoreResponse
import com.example.a20210818_swathi_jatavattu_nycschools.rest.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object SchoolDetailRepository {
    val satScoreMutableLiveData = MutableLiveData<MutableList<SATScoreResponse>>()

    fun getSATScore(): MutableLiveData<MutableList<SATScoreResponse>> {

        val call = RetrofitClient.apiInterface.getSATScores()

        call.enqueue(object : Callback<MutableList<SATScoreResponse>> {
            override fun onFailure(call: Call<MutableList<SATScoreResponse>>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<MutableList<SATScoreResponse>>,
                response: Response<MutableList<SATScoreResponse>>
            ) {

                val data = response.body()

                satScoreMutableLiveData.value = data
            }
        })

        return satScoreMutableLiveData
    }
}