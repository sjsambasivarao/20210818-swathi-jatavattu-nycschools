package com.example.a20210818_swathi_jatavattu_nycschools.repository

import androidx.lifecycle.MutableLiveData
import com.example.a20210818_swathi_jatavattu_nycschools.response.SchoolResponse
import com.example.a20210818_swathi_jatavattu_nycschools.rest.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object SchoolRepository {
    val schoolsMutableLiveData = MutableLiveData<MutableList<SchoolResponse>>()

    fun getSchools(): MutableLiveData<MutableList<SchoolResponse>> {

        val call = RetrofitClient.apiInterface.getNYCSchoolList()

        call.enqueue(object : Callback<MutableList<SchoolResponse>> {
            override fun onFailure(call: Call<MutableList<SchoolResponse>>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<MutableList<SchoolResponse>>,
                response: Response<MutableList<SchoolResponse>>
            ) {

                val data = response.body()

                schoolsMutableLiveData.value = data
            }
        })

        return schoolsMutableLiveData
    }
}