package com.example.a20210818_swathi_jatavattu_nycschools.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SchoolResponse(
    @SerializedName("dbn") val dbn: String,
    @SerializedName("school_name") val schoolName: String,
    @SerializedName("overview_paragraph") val overViewParagraph: String,
    @SerializedName("building_code") val buildingCode: String,
    @SerializedName("location") val location: String,
    @SerializedName("phone_number") val phoneNumber: String,
    @SerializedName("school_email") val schoolEmail: String,
    @SerializedName("website") val webSite: String,
    @SerializedName("total_students") val totalStudents: String
) : Parcelable