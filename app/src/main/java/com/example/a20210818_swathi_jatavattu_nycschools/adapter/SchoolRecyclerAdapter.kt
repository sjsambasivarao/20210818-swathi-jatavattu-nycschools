package com.example.a20210818_swathi_jatavattu_nycschools.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20210818_swathi_jatavattu_nycschools.databinding.ItemSchoolBinding
import com.example.a20210818_swathi_jatavattu_nycschools.response.SchoolResponse
import com.example.a20210818_swathi_jatavattu_nycschools.ui.detail.SchoolDetailActivity
import java.util.*

class SchoolRecyclerAdapter(
    val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var schoolList: MutableList<SchoolResponse> = ArrayList<SchoolResponse>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding =
            ItemSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(context, binding, binding.root)
    }

    fun updateItems(schoolList: MutableList<SchoolResponse>) {
        this.schoolList.clear()
        this.schoolList.addAll(schoolList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return schoolList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SchoolViewHolder) {
            holder.bindData(schoolList[position])
        }
    }

    class SchoolViewHolder(val context: Context, val binding: ItemSchoolBinding, itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bindData(schoolResponse: SchoolResponse) {
            binding.tvSchoolName.text = schoolResponse.schoolName
            binding.tvStudentCount.text = schoolResponse.totalStudents
            binding.tvPhoneNumber.text = schoolResponse.phoneNumber
            binding.tvEmail.text = schoolResponse.schoolEmail
            binding.tvLocation.text = schoolResponse.location

            //school click event
            itemView.setOnClickListener {
                SchoolDetailActivity.launchSchoolActivity(context, schoolResponse)
            }
        }
    }

}