package com.example.a20210818_swathi_jatavattu_nycschools.ui.detail

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.a20210818_swathi_jatavattu_nycschools.R
import com.example.a20210818_swathi_jatavattu_nycschools.databinding.ActivitySchoolDetailBinding
import com.example.a20210818_swathi_jatavattu_nycschools.response.SATScoreResponse
import com.example.a20210818_swathi_jatavattu_nycschools.response.SchoolResponse
import com.example.a20210818_swathi_jatavattu_nycschools.ui.main.SchoolViewModel
import java.util.*

class SchoolDetailActivity : AppCompatActivity() {
    companion object {
        private const val ARG_SCHOOL = "school_data"
        fun launchSchoolActivity(context: Context, schoolResponse: SchoolResponse) {
            val intent = Intent(context, SchoolDetailActivity::class.java)
            intent.putExtra(ARG_SCHOOL, schoolResponse)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivitySchoolDetailBinding
    private var schoolResponse: SchoolResponse? = null
    private lateinit var schoolDetailViewModel: SchoolDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        schoolResponse = intent.getParcelableExtra(ARG_SCHOOL)

        supportActionBar?.title =schoolResponse?.schoolName

        schoolDetailViewModel = ViewModelProvider(this).get(SchoolDetailViewModel::class.java)
        getSATScore()
    }

    private fun getSATScore() {
        binding.progressbar.visibility = View.VISIBLE
        schoolDetailViewModel.getSATScore()
        schoolDetailViewModel.satScoreLiveData?.observe(this, Observer { satScoreResponse ->
            binding.progressbar.visibility = View.GONE

            if (satScoreResponse != null && satScoreResponse.size != 0) {

                //flag to check if data exist for the particular school or not
                var dataExist = false
                for (satScore in satScoreResponse) {

                    //if we found the data then update the view
                    if (satScore.dbn == schoolResponse?.dbn) {
                        binding.detailSchoolName.text = satScore.schoolName
                        binding.detailReadingScore.text =
                            "Reading Score : ${satScore.readingScore}"
                        binding.detailWritingScore.text =
                            "Writing Score : ${satScore.writingScore}"
                        binding.detailMathScore.text = "Math Score : ${satScore.mathScore}"

                        //set the flag to true
                        dataExist = true

                        //come out of the loop
                        break
                    }
                }

                //if data doesn't exist then come out of the activity and show toast
                if (!dataExist) {
                    onError()
                }

            } else {
                //if any error occurs then come out of the activity and show toast
                onError()
            }

        })
    }

    private fun onError() {
        Toast.makeText(this, "No score data found.", Toast.LENGTH_SHORT).show()
        finish()
    }

}