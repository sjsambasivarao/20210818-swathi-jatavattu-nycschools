package com.example.a20210818_swathi_jatavattu_nycschools.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20210818_swathi_jatavattu_nycschools.R
import com.example.a20210818_swathi_jatavattu_nycschools.databinding.ActivityMainBinding
import com.example.a20210818_swathi_jatavattu_nycschools.adapter.SchoolRecyclerAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var schoolViewModel: SchoolViewModel
    private lateinit var schoolRecyclerAdapter: SchoolRecyclerAdapter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpRecyclerView()

        schoolViewModel = ViewModelProvider(this).get(SchoolViewModel::class.java)
        getSchools()
    }

    private fun setUpRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.rvSchool.layoutManager = layoutManager
        schoolRecyclerAdapter = SchoolRecyclerAdapter(this)
        binding.rvSchool.adapter = schoolRecyclerAdapter
    }

    private fun getSchools() {
        binding.progressbar.visibility = View.VISIBLE
        schoolViewModel.getSchools()
        schoolViewModel.schoolsLiveData?.observe(this, Observer { schoolResponse ->
            binding.progressbar.visibility = View.GONE
            if (schoolResponse!=null && schoolResponse.size!=0) {
                    schoolRecyclerAdapter.updateItems(schoolResponse)
                    showHideViews(false, "")
            }else{
                showHideViews(true, getString(R.string.error_no_data))
            }
        })
    }

    private fun showHideViews(isListEmpty : Boolean, message : String){
        binding.tvEmpty.visibility = if(isListEmpty)View.VISIBLE else View.GONE
        binding.rvSchool.visibility = if(isListEmpty)View.GONE else View.VISIBLE
        binding.tvEmpty.text = message
    }
}