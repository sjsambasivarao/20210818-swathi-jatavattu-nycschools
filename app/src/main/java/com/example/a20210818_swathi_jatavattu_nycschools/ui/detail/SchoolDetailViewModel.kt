package com.example.a20210818_swathi_jatavattu_nycschools.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20210818_swathi_jatavattu_nycschools.repository.SchoolDetailRepository
import com.example.a20210818_swathi_jatavattu_nycschools.response.SATScoreResponse

class SchoolDetailViewModel : ViewModel() {
    var satScoreLiveData: MutableLiveData<MutableList<SATScoreResponse>>? = null

    /**
     * method to fetch all the schools
     */
    fun getSATScore() : LiveData<MutableList<SATScoreResponse>>? {
        satScoreLiveData = SchoolDetailRepository.getSATScore()
        return satScoreLiveData
    }
}