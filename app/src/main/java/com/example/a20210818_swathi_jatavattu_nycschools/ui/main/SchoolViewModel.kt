package com.example.a20210818_swathi_jatavattu_nycschools.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20210818_swathi_jatavattu_nycschools.repository.SchoolRepository
import com.example.a20210818_swathi_jatavattu_nycschools.response.SchoolResponse

class SchoolViewModel : ViewModel() {
    var schoolsLiveData: MutableLiveData<MutableList<SchoolResponse>>? = null

    /**
     * method to fetch all the schools
     */
    fun getSchools() : LiveData<MutableList<SchoolResponse>>? {
        schoolsLiveData = SchoolRepository.getSchools()
        return schoolsLiveData
    }
}