package com.example.a20210818_swathi_jatavattu_nycschools.rest

import com.example.a20210818_swathi_jatavattu_nycschools.response.SATScoreResponse
import com.example.a20210818_swathi_jatavattu_nycschools.response.SchoolResponse
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("resource/s3k6-pzi2.json")
    fun getNYCSchoolList(): Call<MutableList<SchoolResponse>>

    @GET("resource/f9bf-2cp4.json")
    fun getSATScores(): Call<MutableList<SATScoreResponse>>
}